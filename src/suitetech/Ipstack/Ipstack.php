<?php

namespace suitetech\Ipstack;

require_once __DIR__ . '/IpstackException.php';

class Ipstack
{
    public function __construct($url, $token, $headers = [])
    {
        global $wpdb;

        $this->url = $url;
        $this->token = $token;
        $this->headers = $headers;

        $this->mysqli = $wpdb->dbh;

        if ($this->checkTables() !== true) {
            throw new IpstackException('Undefined Mysql Error');
        }
    }


    public function checkTables()
    {
        if (!empty($_SESSION['Ipstack']['checkTables'])) {
            return true;
        }


        $query = "SHOW TABLES LIKE 'stech_ipstack_cache'";
        $result = $this->mysqli->query($query);
        if ($result === false) {
            return false;
        }

        if ($result->num_rows != 1) {
            $query = "CREATE TABLE `stech_ipstack_cache` (
                      `id` char(36) NOT NULL,
                      `ip` varchar(16) NOT NULL,
                      `lat` double NOT NULL,
                      `lon` double NOT NULL
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            $result = $this->mysqli->query($query);
            if (!$result)
                return false;

            $query = "ALTER TABLE `stech_ipstack_cache`
                          ADD PRIMARY KEY (`id`),
                          ADD KEY `ip` (`ip`);";
            $result = $this->mysqli->query($query);
            if (!$result)
                return false;

            $query = "COMMIT;";
            $result = $this->mysqli->query($query);
            if (!$result)
                return false;
        }

        $_SESSION['Ipstack']['checkTables'] = true;
        return true;
    }


    public function getFromCache($ip)
    {
        if (empty($ip)) {
            return false;
        }

        $ipArr = explode('.', $ip);
        if (!empty($ipArr[3])) {
            $ipArr[3] = '0';
        }

        $ip = implode('.', $ipArr);

        $query = "SELECT * FROM `stech_ipstack_cache` WHERE ip='{$ip}';";

        $result = $this->mysqli->query($query);
        if (!$result)
            return false;

        if($row = $result->fetch_assoc()) {
            return array(
                'lat' => $row['lat'],
                'lon' => $row['lon'],
            );
        }

        return true;
    }

    public function insertToCache($ip, $lat, $lon)
    {
        if (empty($ip) || empty($lat) || empty($lon)) {
            return false;
        }

        $ipArr = explode('.', $ip);
        if (!empty($ipArr[3])) {
            $ipArr[3] = '0';
        }

        $id = Ipstack::create_guid();
        $ip = implode('.', $ipArr);


        $query = "INSERT INTO `stech_ipstack_cache` (`id`, `ip`, `lat`, `lon`) VALUES ('{$id}', '{$ip}', '{$lat}', '{$lon}');";

        $result = $this->mysqli->query($query);
        if (!$result)
            return false;

        if($row = $result->fetch_assoc()) {
            return array(
                'lat' => $row['lat'],
                'lon' => $row['lon'],
            );
        }

        return true;
    }

    public function geoInfoByIP($ip)
    {
        $url = $this->url;

        if (empty($this->token) || empty($url) || empty($ip)) {
            return false;
        }

        $cache = $this->getFromCache($ip);


        if(is_array($cache)) {
            $answer = [];
            $answer['http_code'] = '200';
            $answer['answer'] = [];
            $answer['answer']['latitude'] = $cache['lat'];
            $answer['answer']['longitude'] = $cache['lon'];
            return $answer;

        } else {
            if (substr($url, -1) != '/') {
                $url .= '/' . $ip . '?access_key=' . $this->token;
            } else {
                $url .= $ip . '?access_key=' . $this->token;
            }

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_POST, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($curl, CURLINFO_HEADER_OUT, false);

            if (!empty($this->headers)) {
                curl_setopt($curl, CURLOPT_HEADER, true);
                curl_setopt($curl, CURLOPT_HTTPHEADER, $this->headers);
            }

            $result = curl_exec($curl);
            //echo var_export($result, true);
            $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            curl_close($curl);
            $answer = json_decode($result, true);
            $this->insertToCache($ip, $answer['latitude'], $answer['longitude']);
            return array(
                'http_code' => $httpCode,
                'answer' => $answer,
            );
        }
    }

    private static function ensure_length(&$string, $length)
    {
        $strlen = strlen($string);
        if ($strlen < $length) {
            $string = str_pad($string, $length, '0');
        } elseif ($strlen > $length) {
            $string = substr($string, 0, $length);
        }
    }

    private static function create_guid_section($characters)
    {
        $return = '';
        for ($i = 0; $i < $characters; ++$i) {
            $return .= dechex(mt_rand(0, 15));
        }

        return $return;
    }

    private static function create_guid()
    {
        $microTime = microtime();
        list($a_dec, $a_sec) = explode(' ', $microTime);

        $dec_hex = dechex($a_dec * 1000000);
        $sec_hex = dechex($a_sec);

        Ipstack::ensure_length($dec_hex, 5);
        Ipstack::ensure_length($sec_hex, 6);

        $guid = '';
        $guid .= $dec_hex;
        $guid .= Ipstack::create_guid_section(3);
        $guid .= '-';
        $guid .= Ipstack::create_guid_section(4);
        $guid .= '-';
        $guid .= Ipstack::create_guid_section(4);
        $guid .= '-';
        $guid .= Ipstack::create_guid_section(4);
        $guid .= '-';
        $guid .= $sec_hex;
        $guid .= Ipstack::create_guid_section(6);

        return $guid;
    }

}