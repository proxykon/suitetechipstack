<?php
/**
 * Created by PhpStorm.
 * User: proxykon
 * Date: 10/5/20
 * Time: 3:44 PM
 */

class IpstackException extends Exception
{
    public function __construct($message, $code = 0, Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}